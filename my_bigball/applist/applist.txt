com.oneplus.membership:OPMemberShip.apk::false
com.oplus.blacklistapp:BlackListApp.apk::false
com.oplus.phonenoareainquire:PhoneNOAreaInquireProvider.apk::false
com.android.contacts:Contacts.apk::false
com.android.incallui:InCallUI.apk::false
com.oneplus.gallery:OnePlusGallery.apk::false
com.android.mms:OplusNaMms.apk::false
com.google.android.calendar:CalendarGoogle::true
com.google.android.apps.docs:Drive::true
com.google.android.apps.tachyon:Duo::true
com.google.android.inputmethod.latin:LatinImeGoogle::true
com.google.android.apps.photos:Photos::true
com.google.android.videos:Videos::true
com.google.android.apps.youtube.music:YTMusic::true
com.google.ar.lens:Google_Lens::true
com.tmobile.pr.adapt:AdaptClient::true
com.tmobile.echolocate:Diagnostics::true
com.google.android.apps.restore:GoogleRestore::true
com.google.android.apps.walletnfcrel:Google_Pay_TMOBILE::true
android.autoinstalls.config.oneplus:PlayAutoInstallConfig_OnePlus::true
com.tmobile.rsuadapter.qualcomm:RSU-QualcommAdapter-v1.0.15-release.signed::true
com.tmobile.rsuapp:rsu-app-v5.08-release.signed::true
com.tmobile.rsusrv:rsu-sysservice-v1.1.1-release.signed::true
com.tmobile.pr.mytmobile:tmoapp-preload-release-universal-8.8.1.83914-signed::true
