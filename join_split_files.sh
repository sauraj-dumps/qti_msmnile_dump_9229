#!/bin/bash

cat system/system/apex/com.google.android.art.apex.* 2>/dev/null >> system/system/apex/com.google.android.art.apex
rm -f system/system/apex/com.google.android.art.apex.* 2>/dev/null
cat my_product/priv-app/OnePlusCamera/OnePlusCamera.apk.* 2>/dev/null >> my_product/priv-app/OnePlusCamera/OnePlusCamera.apk
rm -f my_product/priv-app/OnePlusCamera/OnePlusCamera.apk.* 2>/dev/null
cat my_product/app/OPWallpaperResources-OP7TPro/OPWallpaperResources-OP7TPro.apk.* 2>/dev/null >> my_product/app/OPWallpaperResources-OP7TPro/OPWallpaperResources-OP7TPro.apk
rm -f my_product/app/OPWallpaperResources-OP7TPro/OPWallpaperResources-OP7TPro.apk.* 2>/dev/null
cat my_product/app/OPLiveWallpaper/OPLiveWallpaper.apk.* 2>/dev/null >> my_product/app/OPLiveWallpaper/OPLiveWallpaper.apk
rm -f my_product/app/OPLiveWallpaper/OPLiveWallpaper.apk.* 2>/dev/null
cat modem/image/sdx55m/qdsp6sw.mbn.* 2>/dev/null >> modem/image/sdx55m/qdsp6sw.mbn
rm -f modem/image/sdx55m/qdsp6sw.mbn.* 2>/dev/null
cat recovery.img.* 2>/dev/null >> recovery.img
rm -f recovery.img.* 2>/dev/null
cat my_heytap/priv-app/Velvet/Velvet.apk.* 2>/dev/null >> my_heytap/priv-app/Velvet/Velvet.apk
rm -f my_heytap/priv-app/Velvet/Velvet.apk.* 2>/dev/null
cat my_heytap/priv-app/GmsCore/GmsCore.apk.* 2>/dev/null >> my_heytap/priv-app/GmsCore/GmsCore.apk
rm -f my_heytap/priv-app/GmsCore/GmsCore.apk.* 2>/dev/null
cat my_heytap/app/SpeechServicesByGoogle/SpeechServicesByGoogle.apk.* 2>/dev/null >> my_heytap/app/SpeechServicesByGoogle/SpeechServicesByGoogle.apk
rm -f my_heytap/app/SpeechServicesByGoogle/SpeechServicesByGoogle.apk.* 2>/dev/null
cat my_heytap/app/TrichromeLibrary/TrichromeLibrary.apk.* 2>/dev/null >> my_heytap/app/TrichromeLibrary/TrichromeLibrary.apk
rm -f my_heytap/app/TrichromeLibrary/TrichromeLibrary.apk.* 2>/dev/null
cat my_heytap/app/Gmail2/Gmail2.apk.* 2>/dev/null >> my_heytap/app/Gmail2/Gmail2.apk
rm -f my_heytap/app/Gmail2/Gmail2.apk.* 2>/dev/null
cat my_heytap/app/WebViewGoogle/WebViewGoogle.apk.* 2>/dev/null >> my_heytap/app/WebViewGoogle/WebViewGoogle.apk
rm -f my_heytap/app/WebViewGoogle/WebViewGoogle.apk.* 2>/dev/null
cat my_heytap/app/Maps/Maps.apk.* 2>/dev/null >> my_heytap/app/Maps/Maps.apk
rm -f my_heytap/app/Maps/Maps.apk.* 2>/dev/null
cat my_heytap/app/YouTube/YouTube.apk.* 2>/dev/null >> my_heytap/app/YouTube/YouTube.apk
rm -f my_heytap/app/YouTube/YouTube.apk.* 2>/dev/null
cat system_ext/priv-app/SystemUI/SystemUI.apk.* 2>/dev/null >> system_ext/priv-app/SystemUI/SystemUI.apk
rm -f system_ext/priv-app/SystemUI/SystemUI.apk.* 2>/dev/null
cat system_ext/priv-app/Settings/Settings.apk.* 2>/dev/null >> system_ext/priv-app/Settings/Settings.apk
rm -f system_ext/priv-app/Settings/Settings.apk.* 2>/dev/null
cat system_ext/apex/com.android.vndk.v30.apex.* 2>/dev/null >> system_ext/apex/com.android.vndk.v30.apex
rm -f system_ext/apex/com.android.vndk.v30.apex.* 2>/dev/null
cat my_bigball/priv-app/OnePlusGallery/OnePlusGallery.apk.* 2>/dev/null >> my_bigball/priv-app/OnePlusGallery/OnePlusGallery.apk
rm -f my_bigball/priv-app/OnePlusGallery/OnePlusGallery.apk.* 2>/dev/null
cat my_bigball/app/LatinImeGoogle/LatinImeGoogle.apk.* 2>/dev/null >> my_bigball/app/LatinImeGoogle/LatinImeGoogle.apk
rm -f my_bigball/app/LatinImeGoogle/LatinImeGoogle.apk.* 2>/dev/null
cat my_bigball/app/Photos/Photos.apk.* 2>/dev/null >> my_bigball/app/Photos/Photos.apk
rm -f my_bigball/app/Photos/Photos.apk.* 2>/dev/null
cat my_stock/del-app/OPBreathMode/OPBreathMode.apk.* 2>/dev/null >> my_stock/del-app/OPBreathMode/OPBreathMode.apk
rm -f my_stock/del-app/OPBreathMode/OPBreathMode.apk.* 2>/dev/null
cat my_stock/del-app/CanvasResources/CanvasResources.apk.* 2>/dev/null >> my_stock/del-app/CanvasResources/CanvasResources.apk
rm -f my_stock/del-app/CanvasResources/CanvasResources.apk.* 2>/dev/null
cat boot.img.* 2>/dev/null >> boot.img
rm -f boot.img.* 2>/dev/null
