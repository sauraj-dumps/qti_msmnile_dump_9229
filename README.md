## qssi-user 12 SKQ1.211113.001 1677152667001 release-keys
- Manufacturer: qualcomm
- Platform: msmnile
- Codename: msmnile
- Brand: qti
- Flavor: qssi-user
- Release Version: 12
- Kernel Version: 4.14.180
- Id: SKQ1.211113.001
- Incremental: 1677152667001
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Locale: en-US
- Screen Density: undefined
- Fingerprint: qti/msmnile/msmnile:12/SKQ1.210216.001/1676623833591:user/release-keys
- OTA version: 
- Branch: qssi-user-12-SKQ1.211113.001-1677152667001-release-keys
- Repo: qti_msmnile_dump_9229
